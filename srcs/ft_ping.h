/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ping.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fgarault <fgarault@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/15 16:33:41 by fgarault          #+#    #+#             */
/*   Updated: 2022/11/18 11:33:09 by fgarault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PING_H
# define FT_PING_H

# include <arpa/inet.h>
# include <sys/socket.h>
# include <sys/types.h>
# include <sys/time.h>
# include <netinet/ip_icmp.h> // type  icmphdr, icmp
# include <netdb.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <signal.h>
# include <unistd.h>

/* *********************************** ERROR ********************************* */
# define USAGE_ERROR 		1
# define INVALID_ARGUMENT	2
# define ADDR_REQUIRED		3

/*
**  [IP   header : 20 bytes]
**  [ICMP header : 8 bytes + ttl : 8 bytes]
**  [ICMP payload: 48 bytes] 
*/
# define IP_HEADER			20
# define ICMP_HEADER		8
# define ICMP_PAYLOAD		48

# define MUT				1500
# define IPV4_SIZE			15

typedef struct				s_option
{
	u_char					c;
	u_char					v;
	u_char					t;
	int						datalen;
	int						ttl;
	u_char					bool_dns;
	u_char					interval;
	char					*target_addr;
}							t_option;

typedef struct				s_resolve_dns
{
	char					ip_addr[IPV4_SIZE];
	char					hostname[NI_MAXHOST];
}							t_resolve_dns;

typedef struct 				s_packet
{
	int						sockfd;
	int						len;
	char					sndbuf[MUT];
	struct sockaddr			*send;
	u_char					seq;
}							t_packet;

typedef struct				s_stats
{
	u_char					p_recv;
	double					t_max;
	double					t_min;
	double					t_sum;
	double					t_sqr_sum;
	struct timeval			t_start;
}							t_stats;

typedef struct				s_ping
{
	t_option				opt;
	t_resolve_dns			dns;
	t_packet				pckt;
	t_stats					stats;
}							t_ping;

extern t_ping				g_ping;

/***************************** Parser ******************************/
void						parse_args(char **av);
t_ping						init_global(void);

/*************************** GetAddrInfo ***************************/
void						dns_lookup(void);
int							open_icmp_socket(void);

/*************************** send/recv *****************************/
void						sig_alarm(void);
void						sig_end(void);
void						send_v4(void);
void						recv_icmp(void);

/****************************** Print ******************************/
void						print_usage(int code_err);
void						print_statistics(void);
void						print_echo_reply(struct ip *ip, struct icmp *icmp,
									int icmplen, double rtt);
void						print_verbose(struct icmp *icmp, int icmplen);
void						print_intro_info(void);

/****************************** Libft ******************************/
int							ft_atoi(const char *str);
void						ft_bzero(void *s, size_t n);
void						*ft_memset(void *b, int c, size_t len);
char						*ft_strcpy(char *dst, const char *src);
int							ft_strlen(char *str);

/****************************** math ******************************/
double						mean(void);
double						square(double value);
float						square_root(float nb);
double						deviation(void);
void						tvsub(struct timeval *out, struct timeval *in);

/**************************** Debug ******************************/
void						print_sockaddr(struct sockaddr *addr);
void						print_global(void);

#endif