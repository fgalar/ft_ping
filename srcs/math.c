/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fgarault <fgarault@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/03 11:19:32 by fgarault          #+#    #+#             */
/*   Updated: 2022/11/18 11:15:53 by fgarault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

void	tvsub(struct timeval *recv, struct timeval *send)
{
	if ((recv->tv_usec -= send->tv_usec) < 0)
	{
		--recv->tv_sec;
		recv->tv_usec += 1000000;
	}
	recv->tv_sec -= send->tv_sec;
}

double	mean(void)
{
	return (g_ping.stats.t_sum / g_ping.pckt.seq);
}

double	square(double value)
{
	return (value * value);
}

float	square_root(float nb)
{
	const double	accuracy = 0.0001;
	double			lower;
	double			upper;
	double			sqrt;

	if (nb < 0)
		return (-1);
	if (nb == 0)
		return (0.0);
	if (nb < 1)
	{
		lower = nb;
		upper = 1;
	}
	else
	{
		lower = 1;
		upper = nb;
	}
	while ((upper - lower) > accuracy)
	{
		sqrt = (lower + upper) / 2;
		if (square(sqrt) > nb)
			upper = sqrt;
		else
			lower = sqrt;
	}
	return ((lower + upper) / 2);
}

double	deviation(void)
{
	double	sqr_sum;
	double	sum;

	sqr_sum = g_ping.stats.t_sqr_sum / g_ping.pckt.seq;
	sum = g_ping.stats.t_sum / g_ping.pckt.seq;
	return (square_root(sqr_sum - square(sum)));
}
