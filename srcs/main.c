/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fgarault <fgarault@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/09 13:49:33 by fgarault          #+#    #+#             */
/*   Updated: 2022/11/18 11:26:47 by fgarault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

t_ping	g_ping;

static void	ping_loop(void)
{
	g_ping.pckt.sockfd = open_icmp_socket();
	sig_alarm();
	gettimeofday(&g_ping.stats.t_start, NULL);
	recv_icmp();
}

int	main(int ac, char **av)
{
	if (ac < 2)
		print_usage(USAGE_ERROR);
	else
	{
		g_ping = init_global();
		parse_args(av);
		signal(SIGALRM, (void *)sig_alarm);
		signal(SIGINT, (void *)sig_end);
		dns_lookup();
		ping_loop();
	}
	
	return (0);
}