/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getaddrinfo.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fgarault <fgarault@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/09 13:49:33 by fgarault          #+#    #+#             */
/*   Updated: 2022/11/18 11:29:56 by fgarault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

void	reverse_dns_lookup(struct sockaddr *addr)
{
	char	buf[NI_MAXHOST];

	getnameinfo(
		(struct sockaddr *)addr, sizeof(struct sockaddr_in),
		buf, sizeof(buf),
		NULL, 0, 
		NI_NOFQDN);
	ft_strcpy(g_ping.dns.hostname, buf);
}

void	get_addr_info(struct addrinfo *result)
{
	char		addrbuf[INET_ADDRSTRLEN + 1];
	const char	*addr;

	addr = inet_ntop(result->ai_family,
			&(((struct sockaddr_in *)result->ai_addr)->sin_addr),
			addrbuf, sizeof addrbuf);
	if (addr)
	{
		ft_strcpy(g_ping.dns.ip_addr, addr);
		g_ping.pckt.send = result->ai_addr;
		g_ping.pckt.len = result->ai_addrlen;
		if ((g_ping.opt.bool_dns))
			reverse_dns_lookup(result->ai_addr);
		print_intro_info();
	}
	else
	{
		fprintf(stderr, "%s: %s.\n", g_ping.opt.target_addr, addr);
		exit(EXIT_FAILURE);
	}
}

void	dns_lookup(void)
{
	int				error;
	struct addrinfo	*result;


	if (!g_ping.opt.target_addr)
		print_usage(ADDR_REQUIRED);
	error = getaddrinfo(g_ping.opt.target_addr, NULL, NULL, &result);
	if (error)
	{
		fprintf(stderr, "ft_ping: %s: %s.\n", g_ping.opt.target_addr,
			gai_strerror(error));
		exit(EXIT_FAILURE);
	}
	else
		get_addr_info(result);
	freeaddrinfo(result);
}
