/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fgarault <fgarault@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/18 07:47:36 by fgarault          #+#    #+#             */
/*   Updated: 2022/11/18 10:32:22 by fgarault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

static void	init_option_info(t_option *opt)
{
	opt->c = 255;
	opt->v = 0;
	opt->t = 0;
	opt->datalen = ICMP_HEADER + ICMP_PAYLOAD;
	opt->ttl = 64;
	opt->bool_dns = 1;
	opt->interval = 1;
	opt->target_addr = NULL;
}

static void	init_dns_info(t_resolve_dns *dns)
{
	ft_bzero(dns->ip_addr, IPV4_SIZE);
	ft_bzero(dns->hostname, NI_MAXHOST);
}

static void	init_packet_info(t_packet *pckt)
{
	pckt->sockfd = 0;
	pckt->len = 0;
	pckt->seq = 0;
	ft_bzero(&pckt->sndbuf, MUT);
	ft_bzero(&pckt->send, sizeof(struct sockaddr));
}

static void	init_timeval(struct timeval *tv)
{
	tv->tv_sec = 0;
	tv->tv_usec = 0;
}

static void	init_statistics_info(t_stats *stats)
{
	stats->p_recv = 0;
	stats->t_max = 0;
	stats->t_min = 0;
	stats->t_sum = 0;
	stats->t_sqr_sum = 0;
	init_timeval(&stats->t_start);
}

t_ping	init_global(void)
{
	init_option_info(&g_ping.opt);
	init_dns_info(&g_ping.dns);
	init_packet_info(&g_ping.pckt);
	init_statistics_info(&g_ping.stats);

	return (g_ping);
}
