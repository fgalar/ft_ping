/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fgarault <fgarault@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/18 08:45:48 by fgarault          #+#    #+#             */
/*   Updated: 2022/11/18 11:30:40 by fgarault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

void	print_usage(int code_err)
{
	if (code_err == USAGE_ERROR)
		fprintf(stderr, "ft_ping: usage error: Destination address required\n");
	else if (code_err == ADDR_REQUIRED)
		fprintf(stderr, "ft_ping: usage error: Destination address required\n");
	else if (code_err == INVALID_ARGUMENT)
	{
		fprintf(stderr, "ft_ping: invalid argument: '-56': ");
		fprintf(stderr, "out of range: 0 <= value <= 2147483647\n");
	}
	else
	{
		fprintf(stderr,
			"\nUsage\n"
			"  ping [options] <destination>\n"
			"\nOptions:\n"
			"  <destination>    dns name or ip address\n"
			"  -h               print help and exit\n"
			"  -c <count>       stop after <count> replies\n"
			"  -i <interval>    seconds between sending each packet\n"
			"  -s <size>        use <size> as number of data bytes to be sent\n"
			"  -t <ttl>         define time to live\n"
			"  -v               verbose output\n"

		);
	}
	exit(EXIT_FAILURE);
}

void print_intro_info(void)
{
	printf("FT_PING %s (%s) ", g_ping.opt.target_addr, g_ping.dns.ip_addr);
	printf("%d(%d) bytes of data.\n", g_ping.opt.datalen, g_ping.opt.datalen + 28);
}

void	print_echo_reply(struct ip *ip, struct icmp *icmp, int icmplen, double rtt)
{
	char	*trunc;

	if (icmplen != g_ping.opt.datalen + 8)
		trunc = "truncated";
	else
		trunc = NULL;
	if (!trunc)
		printf("%d bytes from %s: seq=%u, ttl=%d, time=%.2f ms\n",
			icmplen, g_ping.opt.bool_dns == 1 ? g_ping.dns.hostname: g_ping.dns.ip_addr,
			icmp->icmp_seq, ip->ip_ttl, rtt);
	else
		printf("%d bytes from %s: seq=%u, ttl=%d %s\n",
			icmplen, g_ping.opt.bool_dns == 1 ? g_ping.dns.hostname: g_ping.dns.ip_addr,
			icmp->icmp_seq, ip->ip_ttl, trunc);
	g_ping.stats.p_recv++;
}

void	print_verbose(struct icmp *icmp, int icmplen)
{
	if (g_ping.opt.bool_dns == 1)
		printf("%d bytes from %s: type = %d, code = %d\n", icmplen,
			g_ping.dns.hostname, icmp->icmp_type, icmp->icmp_code);
	else
		printf("%d bytes from %s: type = %d, code = %d\n", icmplen,
			g_ping.dns.ip_addr, icmp->icmp_type, icmp->icmp_code);
}

void	print_statistics(void)
{
	struct timeval	end;
	double			elapse;
	int				lost_packet;

	if (g_ping.stats.t_start.tv_sec > 0)
	{
		gettimeofday(&end, NULL);
		tvsub(&end, &g_ping.stats.t_start);
		elapse = (end.tv_sec * 1000) + (end.tv_usec / 1000);
	}
	lost_packet = g_ping.pckt.seq - g_ping.stats.p_recv;
	printf("\n--- %s ft_ping statistics ---\n", g_ping.opt.target_addr);
	printf("%d packets transmitted, %d received ", g_ping.pckt.seq, g_ping.stats.p_recv);
	printf("%d%% packet loss, ", (lost_packet * 100) / g_ping.pckt.seq);
	printf("time %.0f ms\n", elapse = (g_ping.pckt.seq > 1 )? elapse - 1000 * g_ping.opt.interval: 0);
	if (g_ping.stats.t_min && g_ping.stats.t_max)
		printf("rtt min/avg/max/mdev = %.3f/%.3f/%.3f/%.3f ms\n",
			g_ping.stats.t_min, mean(), g_ping.stats.t_max, deviation());
}
