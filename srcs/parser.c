/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fgarault <fgarault@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/07 06:26:45 by fgarault          #+#    #+#             */
/*   Updated: 2022/11/18 11:26:27 by fgarault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

int	get_opt(char **av)
{
	int	arg;

	arg = 0;
	if (av[1])
		arg = ft_atoi(av[1]);
	else
		print_usage(USAGE_ERROR);
	return (arg);
}

void	parse_args(char **av)
{
	int	i;

	i = 0;
	while (av[++i])
	{
		if (av[i][0] == '-')
		{
			switch (av[i][1])
			{
				case 'c' :
					g_ping.opt.c = get_opt(&av[i++]);
					break ;
				case 'i':
					g_ping.opt.interval = get_opt(&av[i++]);
					break ;
				case 'n':
					g_ping.opt.bool_dns = 0;
					break ; 
				case 's':
					g_ping.opt.datalen = get_opt(&av[i++]);
					break ;
				case 't':
					g_ping.opt.t = 1;
					g_ping.opt.ttl = get_opt(&av[i++]);
					break ;
				case 'v' :
					g_ping.opt.v = 1;
					break ;
				default :
					print_usage(0);
			}
		}
		else
			g_ping.opt.target_addr = av[i];
	}
}
