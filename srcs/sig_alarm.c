/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sig_alarm.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fgarault <fgarault@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/07 12:31:55 by fgarault          #+#    #+#             */
/*   Updated: 2022/11/18 11:39:54 by fgarault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

void	sig_alarm(void)
{
	if (g_ping.opt.c == g_ping.pckt.seq)
		sig_end();
	else
	{
		send_v4();
		alarm(g_ping.opt.interval);
	}
}

void	sig_end(void)
{
	print_statistics();
	exit(EXIT_SUCCESS);
}
