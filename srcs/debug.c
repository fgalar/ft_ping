/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fgarault <fgarault@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/28 14:44:51 by fgarault          #+#    #+#             */
/*   Updated: 2022/11/18 08:38:30 by fgarault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

void	print_sockaddr(struct sockaddr *addr)
{
	/* ***************************************************** */
	/*   struct sockaddr {                                   */
	/*       ushort sa_family;                               */
	/*       char sa_data[14];                               */
	/*   }                                                   */
	/*                                                       */
	/*   struct in_addr {                                    */  
	/*       unsigned long s_addr;  // load with inet_aton() */
	/*   };                                                  */
	/* ***************************************************** */
	printf("*****************************************\n");
	printf("* Struct sockaddr:\n");
	printf("* family = %d\n", (((struct sockaddr_in *)addr)->sin_family));
	printf("* port = %d\n", (((struct sockaddr_in *)addr)->sin_port));
	printf("* addr = %s\n", inet_ntoa((((struct sockaddr_in *)addr)->sin_addr)));
	printf("*****************************************\n");
}

void	print_flags(void)
{
	/* *****************  flag  **************** */
	/*		u_char		v;                       */
	/*		u_char		c;                       */
	/*		int			datalen;                 */
	/*		char		*target_addr;            */
	/* ***************************************** */
	printf("*****************************************\n");
	printf("* \tflags:\n");
	printf("*****************************************\n");
	printf("* v = %d\n", g_ping.opt.v);
	printf("* c = %d\n", g_ping.opt.c);
	printf("* datalen = %d\n", g_ping.opt.datalen);
	printf("* target_addr = %s\n", g_ping.opt.target_addr);
}

void	print_dns_resolution(void)
{
	/* ************** solve dns **************** */
	/*		char		ip_addr[IPV4_SIZE];      */
	/*		char		hostname[NI_MAXHOST];    */
	/*		int			sockfd;                  */
	/* ***************************************** */
	printf("*****************************************\n");
	printf("* \tDNS:\n");
	printf("*****************************************\n");
	printf("* ip_addr: %s\n", g_ping.dns.ip_addr);
	printf("* hostname: %s\n", g_ping.dns.hostname);
	printf("*****************************************\n");
}

void	print_global(void)
{
	/* ***************************************************** */
	/*	typedef struct s_option {                            */
	/*		***********  flag  **********                    */
	/*		********* solve dns *********                    */
	/*		struct sockaddr			*send;                   */
	/*		int						len;                     */
	/*		char					sndbuf[MUT];             */
	/*		u_char					seq;                     */
	/*		******** packets stats ********                  */
	/*		u_char					p_recv;                  */
	/*		double					t_max;                   */
	/*		double					t_min;                   */
	/*		double					t_sum;                   */
	/*		double					t_sqr_sum;               */
	/*		struct timeval			t_start;                 */
	/*	}							t_option;                */
	/* ***************************************************** */
	printf("*****************************************\n");
	printf("* Struct g_ping:\n");
	print_flags();
	print_dns_resolution();
	printf("*****************************************\n");
}
