/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   send_v4.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fgarault <fgarault@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/07 11:49:06 by fgarault          #+#    #+#             */
/*   Updated: 2022/11/18 10:54:37 by fgarault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

uint16_t	in_cksum(uint16_t *addr, int len)
{
	int			nleft;
	uint32_t	sum;
	uint16_t	*w;
	uint16_t	answer;

	nleft = len;
	sum = 0;
	w = addr;
	answer = 0;
	while (nleft > 1)
	{
		sum += *w++;
		nleft -= 2;
	}
	if (nleft == 1)
	{
		*(unsigned char *)(&answer) = *(unsigned char *)w;
		sum += answer;
	}
	sum = (sum >> 16) + (sum & 0xffff);
	sum += (sum >> 16);
	answer = ~sum;
	return (answer);
}

int	open_icmp_socket(void)
{
	int	size;
	int	sock_fd;

	size = 60 * 1024;
	sock_fd = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if (sock_fd < 0)
	{
		fprintf(stderr, "Socket file descriptor not received.");
		exit(EXIT_FAILURE);
	}
	if (setsockopt(sock_fd, SOL_SOCKET, SO_RCVBUF, &size, sizeof(size)) < 0)
	{
		printf("Failed to set revbuff.");
		exit(EXIT_FAILURE);
	}
	if (g_ping.opt.t == 1 && (setsockopt(sock_fd, IPPROTO_IP, IP_TTL,
				&g_ping.opt.ttl, sizeof(g_ping.opt.ttl))) < 0)
	{
		printf("Failed to set ttl.");
		exit(EXIT_FAILURE);
	}
	return (sock_fd);
}

void	send_v4(void)
{
	int			len;
	struct icmp	*icmp;
	int			ret;

	g_ping.pckt.seq++;
	icmp = (struct icmp *)g_ping.pckt.sndbuf;
	icmp->icmp_type = ICMP_ECHO;
	icmp->icmp_code = 0;
	icmp->icmp_id = getpid();
	icmp->icmp_seq = g_ping.pckt.seq;
	ft_memset(icmp->icmp_data, 0, g_ping.opt.datalen);
	gettimeofday((struct timeval *)icmp->icmp_data, NULL);
	len = 8 + g_ping.opt.datalen;
	icmp->icmp_cksum = in_cksum((u_short *) icmp, len);
	ret = sendto(g_ping.pckt.sockfd, g_ping.pckt.sndbuf, len, MSG_CONFIRM,
			g_ping.pckt.send, g_ping.pckt.len);
	if (ret <= 0)
	{
		fprintf(stderr, "Sending packet failed.\n");
		exit(EXIT_FAILURE);
	}
}
