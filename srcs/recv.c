/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recv.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fgarault <fgarault@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/09 13:49:33 by fgarault          #+#    #+#             */
/*   Updated: 2022/11/18 11:15:59 by fgarault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

static double	get_roundtrip_delay(struct icmp *icmp, struct timeval *tvrecv)
{
	struct timeval	*tvsend;
	double			rtt;

	tvsend = (struct timeval *)icmp->icmp_data;
	tvsub(tvrecv, tvsend);
	rtt = tvrecv->tv_sec * 1000.0 + tvrecv->tv_usec / 1000.0;
	g_ping.stats.t_sum += rtt;
	g_ping.stats.t_sqr_sum += square(rtt);
	if (g_ping.stats.t_min == 0 || g_ping.stats.t_min > rtt)
		g_ping.stats.t_min = rtt;
	if (g_ping.stats.t_max == 0 || g_ping.stats.t_max < rtt)
		g_ping.stats.t_max = rtt;
	return (rtt);
}

static void	proc_v4(char *ptr, ssize_t len, struct timeval *tvrecv)
{
	int			iplen;
	int			icmplen;
	struct ip	*ip;
	struct icmp	*icmp;
	double		rtt;

	ip = (struct ip *)ptr;
	iplen = ip->ip_hl << 2;
	if (ip->ip_p != IPPROTO_ICMP)
		return ;
	icmp = (struct icmp *)(ptr + iplen);
	icmplen = len - iplen;
	if (icmplen < 8)
		return ;
	if (icmp->icmp_type == ICMP_ECHOREPLY)
	{
		if (icmp->icmp_id != getpid())
			return ;
		rtt = get_roundtrip_delay(icmp, tvrecv);
		print_echo_reply(ip, icmp, icmplen, rtt);
	}
	else if (g_ping.opt.v)
		print_verbose(icmp, icmplen);
}

void	recv_icmp(void)
{
	char			recvbuf[1500];
	struct msghdr	msg;
	struct iovec	iov;
	struct timeval	tv;
	int				n;

	ft_bzero(recvbuf, sizeof(recvbuf));
	iov.iov_base = recvbuf;
	iov.iov_len = sizeof(recvbuf);
	msg.msg_name = g_ping.pckt.sndbuf;
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = NULL;
	while (1)
	{	
		msg.msg_namelen = g_ping.pckt.len;
		msg.msg_controllen = 0;
		n = recvmsg(g_ping.pckt.sockfd, &msg, 0);
		if (n < 0) {
			printf("recverr");
		} else {
			gettimeofday(&tv, NULL);
			proc_v4(recvbuf, n, &tv);
		}
	}
}
