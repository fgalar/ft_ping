# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fgarault <fgarault@42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/09/15 16:30:47 by fgarault          #+#    #+#              #
#    Updated: 2022/11/18 10:54:22 by fgarault         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= ft_ping

CC		= gcc
CFLAGS	= -Wall -Wextra -Werror
SRCDIR	= srcs
SRCS	= main.c init.c parser.c getaddrinfo.c libft.c sig_alarm.c recv.c send_v4.c debug.c math.c print.c
HEADER	= srcs/ft_ping.h
OBJDIR	= objs
OBJS	= $(addprefix $(OBJDIR)/, $(SRCS:.c=.o))

RED		= \033[1;31m
GREEN	= \033[1;32m
YELLOW	= \033[1;33m
PURPLE	= \033[1;34m
BLUE	= \033[1;36m
WHITE	= \033[1;37m

all : $(NAME)

$(NAME) : $(OBJS)
	@$(CC) $(CFLAGS) -o $@ $(OBJS)

$(OBJS): | $(OBJDIR)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@$(CC) $(CFLAGS) -o $@ -c $< 

$(OBJDIR):
	@mkdir $@

norme :
	 @norminette -R CheckForbiddenSourceHeader $(SRCDIR)/$(SRCS)

clean :
	@rm -rf $(OBJDIR)

leaks : all
	@valgrind --tool=memcheck --leak-check=full ./$(NAME) locahost

fclean : clean
	@rm -rf $(NAME)

re : fclean all
